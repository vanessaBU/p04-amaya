package vamaya1.lizardlunch;

import java.util.Random;

/**
 * Created by vanessa on 3/11/2017.
 */

public class Sand {

    private int x, y; // coordinates
    private int maxX, minX, maxY, minY;
    private int speed;

    public Sand(int screenX, int screenY) {
        // generate random coordinates inside screen
        maxX = screenX;
        maxY = screenY;
        minX = minY = 0;
        Random generator = new Random();
        x = generator.nextInt(maxX);
        y = generator.nextInt(maxY);
    }

    public void update(int playerSpeed) {
        x -= playerSpeed; // decrease x coordinate to move sand right to left
        x -= speed;
        // if sand reaches left edge of screen then move to right edge
        if (x < 0) {
            x = maxX;
            Random generator = new Random();
            y = generator.nextInt(maxY);
            speed = generator.nextInt(15);
        }
    }

    public float getSandWidth() { // random sand width
        float minX = 1.0f;
        float maxX = 4.0f;
        Random rand = new Random();
        float finalX = rand.nextFloat() * (maxX - minX) + minX;
        return finalX;
    }

    // getters
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
}
