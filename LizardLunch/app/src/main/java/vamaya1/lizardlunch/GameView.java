package vamaya1.lizardlunch;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.view.Display;
import android.view.SurfaceView;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import android.view.MotionEvent;
import android.view.WindowManager;

import java.util.ArrayList;

/**
 * add sound effect: https://stackoverflow.com/questions/10451092/how-to-play-a-sound-effect-in-android
 */

public class GameView extends SurfaceView implements Runnable {

    volatile boolean playing; // track if game is playing or not
    private Thread gameThread = null; // game thread
    private Gecko gecko; // add main character to class
    // drawing objects
    private Paint paint;
    private Canvas canvas;
    private SurfaceHolder surfaceHolder;
    // background sand
    private ArrayList<Sand> sands = new ArrayList<Sand>();
    // predator and prey
    private Bug[] bugs;
    private Dragon[] dragons;
    private int bugCount = 3;
    private int dragonCount = 1;
    // om nom visuals and sound effects
    private Chomp chomp;
    private Splat splat;
    MediaPlayer chompSound;
    MediaPlayer splatSound;
    // game over
    private static boolean dead;
    private Bitmap gameOver;

    public GameView(Context context, int screenX, int screenY) {

        super(context);

        // init player
        gecko = new Gecko(context, screenX, screenY);
        // init drawing objects
        surfaceHolder = getHolder();
        paint = new Paint();
        // add background sand
        int sandParticles = 200;
        for (int i = 0; i < sandParticles; i++) {
            Sand s  = new Sand(screenX, screenY);
            sands.add(s);
        }
        // prey objects
        bugs = new Bug[bugCount];
        for(int i = 0; i < bugCount; i++){
            bugs[i] = new Bug(context, screenX, screenY);
        }
        chomp = new Chomp(context);
        chompSound = MediaPlayer.create(context, R.raw.chomp);
        // predator objects
        dragons = new Dragon[dragonCount];
        for(int i = 0; i < dragonCount; i++){
            dragons[i] = new Dragon(context, screenX, screenY);
        }
        splat = new Splat(context);
        splatSound = MediaPlayer.create(context, R.raw.splat);
        // game over
        dead = false;
        gameOver = BitmapFactory.decodeResource(context.getResources(), R.drawable.gameover);
    }

    @Override
    public void run() {

        while (playing) {
            update(); // update frame
            draw(); // draw frame
            control();
        }
    }

    private void update() {

        gecko.update(); // update position
        // set om nom visual outside screen
        chomp.setX(-250);
        chomp.setY(-250);
        splat.setX(-250);
        splat.setY(-250);
        // update starts to match gecko speed
        for (Sand s : sands) {
            s.update(gecko.getSpeed());
        }
        // update prey
        for(int i = 0; i < bugCount; i++){
            bugs[i].update(gecko.getSpeed()); // match gecko speed
            // detect collision
            if (Rect.intersects(gecko.getDetectCollision(), bugs[i].getDetectCollision())) {
                chomp.setX(bugs[i].getX()); // display visual
                chomp.setY(bugs[i].getY());
                chompSound.start(); // play sound effect
                bugs[i].setX(-200); // move off screen
            }
        }
        // update predator
        for(int i = 0; i < dragonCount; i++){
            dragons[i].update(gecko.getSpeed()); // match gecko speed
            // detect collision
            if (Rect.intersects(gecko.getDetectCollision(), dragons[i].getDetectCollision())) {
                splat.setX(gecko.getX()); // display visual
                splat.setY(gecko.getY());
                splatSound.start(); // play sound effect
                // game over
                gecko.setX(-400); // move gecko off screen
                dead = true;
            }
        }
    }

    private void draw() {

        if (surfaceHolder.getSurface().isValid()) { // check if surface is valid
            canvas = surfaceHolder.lockCanvas(); // lock canvas
            canvas.drawColor(Color.argb(255, 219, 209, 180)); // canvas background color sand
            paint.setColor(Color.argb(255, 139, 69, 19)); // paint color for sand particles brown
            // draw sand particles
            for (Sand s : sands) {
                paint.setStrokeWidth(s.getSandWidth());
                canvas.drawPoint(s.getX(), s.getY(), paint);
            }
            // draw gecko
            canvas.drawBitmap(
                    gecko.getBitmap(),
                    gecko.getX(),
                    gecko.getY(),
                    paint
            );
            // draw prey
            for (int i = 0; i < bugCount; i++) {
                canvas.drawBitmap(
                        bugs[i].getBitmap(),
                        bugs[i].getX(),
                        bugs[i].getY(),
                        paint
                );
            }
            // draw predator
            for (int i = 0; i < dragonCount; i++) {
                canvas.drawBitmap(
                        dragons[i].getBitmap(),
                        dragons[i].getX(),
                        dragons[i].getY(),
                        paint
                );
            }
            // draw om nom visuals
            canvas.drawBitmap(
                    chomp.getBitmap(),
                    chomp.getX(),
                    chomp.getY(),
                    paint
            );
            canvas.drawBitmap(
                    splat.getBitmap(),
                    splat.getX(),
                    splat.getY(),
                    paint
            );
            // draw game over
            if (dead) {
                WindowManager wm = (WindowManager) this.getContext().getSystemService(Context.WINDOW_SERVICE);
                Display display = wm.getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int width = size.x / 2 - gameOver.getWidth() / 2;
                int height = size.y / 2 - gameOver.getHeight() / 2;
                canvas.drawBitmap(
                    gameOver,
                    width,
                    height,
                    paint
                );
                ((Activity)this.getContext()).finish();
            }
            surfaceHolder.unlockCanvasAndPost(canvas); // unlock canvas
        }
    }

    private void control() {
        try {
            gameThread.sleep(17);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void pause() {
        playing = false;
        try {
            gameThread.join(); // stop thread
        }
        catch (InterruptedException e) {}
    }

    public void resume() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP: // user releases screen
                gecko.stopBoosting(); // stop boost
                break;
            case MotionEvent.ACTION_DOWN: // user presses screen
                gecko.setBoosting(); // apply boost
                break;
        }
        return true;
    }
}
