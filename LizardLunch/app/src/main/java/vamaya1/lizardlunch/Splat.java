package vamaya1.lizardlunch;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class Splat {

    private Bitmap bitmap; // stores image
    private int x, y; // coordinates

    public Splat(Context context) {
        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.splat); // set image
        x = y = -250; // set coordinates outside of screen
    }

    // setters: use to make splat visible on collision
    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }
    /*public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }*/

    // getters
    public Bitmap getBitmap() { return bitmap; }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
}
