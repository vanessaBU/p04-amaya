package vamaya1.lizardlunch;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.View;
import android.widget.ImageButton;
//import android.media.Image;
//import android.widget.Button;

/**
 * following tutorial: https://www.simplifiedcoding.net/android-game-development-tutorial-1/
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageButton buttonPlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); // set orientation to landscape
        buttonPlay = (ImageButton) findViewById(R.id.buttonPlay); // get play button
        buttonPlay.setOnClickListener(this); // add click listener
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(this, GameActivity.class)); // start game activity
    }
}
