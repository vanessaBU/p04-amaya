package vamaya1.lizardlunch;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import java.util.Random;

/**
 * Created by vanessa on 3/11/2017.
 */

public class Dragon {

    private Bitmap bitmap; // store image
    private int x, y; // coordinates
    private int speed = 1; // delta x
    private int maxX, minX, maxY, minY; // coordinates to keep dragon inside screen
    private Rect detectCollision; // collision rect

    public Dragon(Context context, int screenX, int screenY) {

        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.dragon); // set image
        maxX = screenX; // initialize min/max coordinates
        maxY = screenY;
        minX = minY = 0;

        // generate random speed and coordinate
        Random generator = new Random();
        speed = generator.nextInt(6) + 25; // possible outcome between 25-30
        x = screenX;
        y = generator.nextInt(maxY) - bitmap.getHeight();

        detectCollision = new Rect(x, y, bitmap.getWidth(), bitmap.getHeight());
    }

    public void update(int playerSpeed) {
        x -= playerSpeed; // decrease x coordinate to move dragon right to left
        x -= speed;
        // if dragon reaches left edge of screen then move to right edge
        if (x < minX - bitmap.getWidth()) {
            Random generator = new Random();
            speed = generator.nextInt(10) + 10;
            x = maxX;
            y = generator.nextInt(maxY) - bitmap.getHeight();
        }
        // set collision dimensions
        detectCollision.left = x + bitmap.getWidth() / 8;
        detectCollision.top = y + bitmap.getHeight() / 2;
        detectCollision.right = bitmap.getWidth() - detectCollision.left;
        detectCollision.bottom = detectCollision.top + bitmap.getHeight() / 2;
    }

    // setters
    public void setX(int x){
        this.x = x;
    }
    public Rect getDetectCollision() {
        return detectCollision;
    }

    // getters
    public Bitmap getBitmap() {
        return bitmap;
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    /*public int getSpeed() {
        return speed;
    }*/
}
