package vamaya1.lizardlunch;

import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;

public class GameActivity extends AppCompatActivity {

    private GameView gameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        Display display = getWindowManager().getDefaultDisplay(); // get display object

        Point size = new Point(); // get screen resolution into point object
        display.getSize(size);

        gameView = new GameView(this, size.x, size.y); // initialize game view object, pass screen size to game view

        setContentView(gameView); // add game view to content view
    }

    @Override
    protected void onPause() { // pause game if activity is paused
        super.onPause();
        gameView.pause();
    }

    @Override
    protected void onResume() { // resume game when activity resumed
        super.onResume();
        gameView.resume();
    }
}
