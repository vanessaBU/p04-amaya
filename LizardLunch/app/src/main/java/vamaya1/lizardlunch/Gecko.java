package vamaya1.lizardlunch;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

/**
 * Created by vanessa on 3/10/2017.
 */

public class Gecko {

    private Bitmap bitmap; // stores image
    private int x, y; // coordinates
    private int maxX, maxY, minY; // screen bounds
    private int speed = 0; // vertical speed
    private final int MIN_SPEED = 1; // speed bounds
    private final int MAX_SPEED = 20;
    private boolean boosting; // vertical boost
    private final int GRAVITY = -10;
    private Rect detectCollision; // collision rect
    private boolean dead;

    public Gecko(Context context, int screenX, int screenY) {

        x = 75; // starting coordinate
        y = 50;
        speed = 1; // starting speed
        boosting = false; // boost is false until user touches screen

        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.gecko); // set image
        maxY = screenY - bitmap.getHeight(); // calculate max y
        minY = 0; // top edge y point is 0

        detectCollision =  new Rect(x, y, bitmap.getWidth(), bitmap.getHeight());
    }

    public void update() { // update gecko coordinate

        // boost applies speed, otherwise slow down
        if (boosting) {
            speed += 2;
        } else {
            speed -= 5;
        }
        // cap max and min speeds
        if (speed > MAX_SPEED) {
            speed = MAX_SPEED;
        }
        if (speed < MIN_SPEED) {
            speed = MIN_SPEED;
        }
        // apply gravity
        y -= speed + GRAVITY;
        // keep gecko within bounds
        if (y < minY) {
            y = minY;
        }
        if (y > maxY) {
            y = maxY;
        }
        // set collision bounds
        detectCollision.left = x + bitmap.getWidth() / 4;
        detectCollision.top = y + bitmap.getHeight() / 10;
        detectCollision.right = bitmap.getWidth();
        detectCollision.bottom = detectCollision.top + bitmap.getHeight() / 2;
    }

    // boost toggle
    public void setBoosting() {
        boosting = true;
    }
    public void stopBoosting() {
        boosting = false;
    }

    // setters
    public void setX(int x){
        this.x = x;
    }

    // getters
    public Rect getDetectCollision() {
        return detectCollision;
    }
    public Bitmap getBitmap() {
        return bitmap;
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public int getSpeed() {
        return speed;
    }
}
