Lizard Lunch
-------------

This was made in Android Studio following an online tutorial (link referenced in MainActivity).

Objective:
Your main character is a gecko. As the gecko scuttles across the desert in search of tasty bugs to munch on, she must also be wary of scary predators like the bearded dragon and avoid being eaten herself. Tap on screen (or click) to move the gecko up and release to move down to catch prey or avoid predators.

Problems:
I could not get CountDownTimer to work without crashing the application. I removed it, so if you die, the game over screen flashes then disappears and returns you to the main screen.